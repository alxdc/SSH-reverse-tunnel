.. SSH-reverse-tunnel documentation master file, created by
   sphinx-quickstart on Tue Apr  9 15:08:32 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SSH-reverse-tunnel's documentation!
==============================================

What is SSH-reverse-tunnel ?
----------------------------
SSH-reverse-tunnel is a reverse SSH tunneling tool. When started, it will open an SSH tunnel from the server to the client
and will expose a server port on the client. The client can then send TCP requests to that port.

Its main use is to access a server ressource behind a firewall or in a network that blocks incoming requests. By using
a reverse tunnel, the server will first establish an outgoing connection that will then be used by the client to access
the wanted ressource.

SSH-reverse-tunnel wraps the SSH tool with some python code used to get configuration values and to ensure that the
connection remains active by reconnecting if needed.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

How to install ?
----------------

Server side
+++++++++++

The python file named ``server.py`` can be used as-is. Otherwise a dockerfile and a docker-compose are provided to create
a docker container running the reverse tunnel server.

Creating the docker container
*****************************
First, the docker image has to be created using the ``dockerfile``. Execute the following command while in the root folder
of the project:

.. code-block:: bash

    docker build -t SSH-reverse-tunnel .

Then the container can be created using ``docker-compose``:

.. code-block:: bash

    docker-compose up -d

Client side
+++++++++++

One the client side, the only need is to have an SSH server running. If you're running a Linux system, you just need to
ensure that the server is running and to add the server key to the ``authorized_keys`` file as described below.

It is also required to set up a port forwarding on your router. The port to be forwared is the SSH client port (default is
22).

Using Windows as a client
*************************

Since release 1809, Windows 10 includes OpenSSH client and server. SSH-reverse-tunnel requires only the server part to be
installed on the Windows client.

To install the server, go to ``Settings``, then ``Apps``, ``Apps and Features`` and ``Manage Optional Features``. Then
select ``Add a feature`` on the top of the page. Locate ``OpenSSH Server`` and click ``Install``.

The settings of the SSH server are stored in a file named ``sshd_config`` which is in ``%programdata%\ssh\sshd_config``.

A firewall rule has to be created to allow incoming SSH connections. To do so, run the following PowerShell command as
Administrator. Change the ``-LocalPort 22`` to match the SSH port you have set.

.. code-block:: PowerShell

    New-NetFirewallRule -Name sshd -DisplayName 'OpenSSH SSH Server'
    -Enabled True -Direction Inbound -Protocol TCP -Action Allow -LocalPort 22

Adding the SSH key
******************

In order to have a fully automated process, the server will use SSH keys to log on the client. Thus, the public key stored
in ``id_rsa.pub`` has to be added to the list of ``authorized_keys`` of the client:

To do so, execute the following command while in the ``keys`` folder on the server:

.. code-block:: bash

    ssh-copy-id -f id_rsa.pub <username>@<client_adress> -p <ssh_port>

If you do not have access to the server but have the public key file, you can add the key running the following command.

On a Linux client:

.. code-block:: bash

    cat id_rsa.pub >> ~/.ssh/authorized_keys

On a Windows client:

.. code-block:: bash

    type id_rsa.pub >> %userprofile%/.ssh/authorized_keys

Restrict SSH user access
++++++++++++++++++++++++
As the server establishes an SSH connection, anyone on the server side can access the client and execute commands there.
It is recommended to create a ``chroot jail`` to lock the user in a specific folder.

On Linux: https://www.cyberciti.biz/faq/debian-ubuntu-restricting-ssh-user-session-to-a-directory-chrooted-jail/

On Windows: https://github.com/PowerShell/Win32-OpenSSH/issues/190

Parameters
----------
The tool will load the required parameters from environment variables.

 * ``forwarded_server_port`` port that will be forwarded to the client
 * ``forwarded_host_ip`` IP of the server side what will be forwarded to the client
 * ``client_address`` URL or IP which used to reach the client
 * ``client_port`` port on the client side that will be used to access the forwarded server port
 * ``client_ssh_port`` SSH port of the client
 * ``client_username`` username to log on the client
 * ``retry_delay`` delay to wait before each connection tentative


When using the docker container, these parameters are already set using the ``docker-compose.yml`` file.
To change a parameter value, simply edit the ``docker-compose.yml`` file and recreate the container. If using the server.py
file without the container, you can edit the settings.conf file to edit the parameters of the tunnel.

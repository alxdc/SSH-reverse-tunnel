#!/usr/bin/env python2.7
'''
SSH tunnel server

The server establishes a SSH reverse tunnel and exposes a given port to the client
'''
import subprocess
from time import sleep
import os
import sys

class Tunnel:
    '''
    Tunnel class
    '''
    def __init__(self):
        '''
        Create a tunnel instance ready to connect
        '''

        container = self.running_in_container()

        if container:
            # Get required environment variables
            self.server_port = os.getenv('server_port')
            self.forwarded_host_ip = os.getenv('forwarded_host_ip')
            self.client_address = os.getenv('client_address')
            self.client_username = os.getenv('client_username')
            self.client_port = os.getenv('client_port')
            self.client_ssh_port = os.getenv('client_ssh_port')
            self.retry_delay = os.getenv('retry_delay')

            if self.retry_delay is not None:
                self.retry_delay == int(self.retry_delay)

        else:
            # Not running in a container. Load parameters from a config file.
            self.server_port = '22'
            self.forwarded_host_ip = '127.0.0.1'
            self.client_address = 'test.com'
            self.client_username = 'root'
            self.client_ssh_port = '22'
            self.retry_delay = 5

        self.process = False
        self.connected = False
        port_host = self.client_port + ':' + self.forwarded_host_ip  + ':' + self.server_port
        target = self.client_username + '@' + self.client_address

        # Create the command required to set the tunnel up
        self.command = ['ssh', '-o', 'ExitOnForwardFailure yes', '-o ConnectTimeout=1', '-o' 'StrictHostKeyChecking no',
                        '-o', 'PasswordAuthentication no', '-NR', port_host, '-i', '/conf/id_rsa' '-p',
                        self.client_ssh_port, target]

        print(self.command)

    def generate_keys(self):
        '''
        Generate a key pair if not done before.
        '''
        # Check if keys exist
        keys_exist = False
        keys_exist = os.path.isfile(self.keys_path + 'id_rsa') and os.path.isfile(self.keys_path + 'id_rsa.pub')

        # Generate keys if they do not exist
        if not keys_exist:
            self.keys_gen_command = ['mkdir', '-p', 'keys', '&&', 'ssh-keygen.exe', '-t', 'rsa', '-f',
                                     'keys/id_rsa', '-q', '-N ""']

    def is_connected(self):
        '''
        Check if tunnel is connected.
        The check is done by verifying that the ssh process is still running.
        The real status of the ssh tunnel is not verified.
        '''
        if not self.process:
            return False

        return self.process.poll() is None

    def connect(self):
        '''
        Establish the reverse ssh tunnel
        '''
        # Infinite loop that continuously tries to create the tunnel if not connected
        while True:
            if not self.is_connected():
                print('Trying to connect...')
                self.process = subprocess.Popen(self.command, shell=False)
                sleep(3)
                self.connected = self.is_connected()

                # Wait 30 seconds before next try
                if not self.is_connected():
                    print('Connection failed. Waiting {} seconds before trying to connect.'.format(self.retry_delay))
                    sleep(self.retry_delay)
                else:
                    print('Tunnel established !')

    def running_in_container(self):
        '''
        Check if the server is running in a container
        '''
        container = os.getenv('AM_I_IN_A_DOCKER_CONTAINER')

        if container is None:
            return False

        return True

if __name__ == '__main__':
    tunnel = Tunnel()
    tunnel.connect()

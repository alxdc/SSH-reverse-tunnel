FROM python:3.7-slim

ADD server.py /

RUN apt-get update && apt-get install -y openssh-client
ENV AM_I_IN_A_DOCKER_CONTAINER True

CMD [ "python", "-u", "server.py" ]